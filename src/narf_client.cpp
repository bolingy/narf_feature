#include <data_collection/collect_data.h>
#include <depth_feature/depth_feature.hpp>

int main(int argc, char **argv)
{
	ros::init(argc, argv, "narf_client");

	ros::NodeHandle n;
	ros::ServiceClient client = n.serviceClient<depth_feature::narf_rect>("narf_rect");



	std::string pc_topic = "/record_data/depth_cloud";
  	sensor_msgs::PointCloud2::ConstPtr sm_cloud_ptr = ros::topic::waitForMessage<sensor_msgs::PointCloud2>(pc_topic);
	if (!sm_cloud_ptr){
    	ROS_ERROR("objectDetection::filter_cloud no PointCloud2 has been received");
 	}
 	std::string ci_topic = "/record_data/depth_info"; 
	sensor_msgs::CameraInfoConstPtr camera_info_msg = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(ci_topic);
	if (!camera_info_msg){
    	ROS_ERROR("no camera info has been received");
 	}
    std::string img_topic = "/record_data/rgb_image"; 
	sensor_msgs::ImageConstPtr img_ptr = ros::topic::waitForMessage<sensor_msgs::Image>(img_topic);
	if (!img_ptr){
    	ROS_ERROR("no image has been received");
 	}
 
 	ROS_INFO("topics all right");
	
	// calculate surface area
	pcl::PCLPointCloud2 pcl_pc2;
	pcl_conversions::toPCL(*sm_cloud_ptr, pcl_pc2);
	pcl::PointCloud<pcl::PointXYZ>::Ptr temp_cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::fromPCLPointCloud2(pcl_pc2, *temp_cloud);
    // filter the nan points
	for (int i=temp_cloud->points.size()-1;i>=0;--i){
		pcl::PointXYZ cur_point = temp_cloud->points[i];
		if(isnan(cur_point.x)||isnan(cur_point.y)||isnan(cur_point.z)){
			temp_cloud->erase(temp_cloud->begin() + i);
		}
	}
    
    // convert to base link
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out(new pcl::PointCloud<pcl::PointXYZ>);
    tf::TransformListener tf_listener;
    ros::Duration d(1);
    d.sleep();
	pcl_conversions::toPCL(ros::Time(0), temp_cloud->header.stamp);
    pcl_ros::transformPointCloud ("base_link", *temp_cloud, *cloud_out,  tf_listener);
	// calculate area from point cloud
	double area;
	DataCollector::get_xy_cloud_area(cloud_out, area);
	ROS_INFO("surface area is: %f", area);
	
	depth_feature::narf_rect srv;
	srv.request.input_cloud = *sm_cloud_ptr;
	srv.request.camera_info = *camera_info_msg;
	srv.request.object_size = area;

	if (client.call(srv))
	{
		// show box
		sensor_msgs::ImagePtr rec_image;
		std::vector<TwoPointRectangle> rectangles;
		for (int i=0;i<srv.response.boxes.size();++i){
			deep_rec::BoxMsg boxMsg = srv.response.boxes[i];
			TwoPointRectangle tpr(boxMsg.box[0], boxMsg.box[1], boxMsg.box[2], 
									boxMsg.box[3], boxMsg.box[4], boxMsg.box[5]);
									
			rectangles.push_back(tpr);
		}
		FourPointRectangle fpr = rectangles[0].get_four_point_rectangle();
		DataCollector::get_rectangle_image(rectangles, *img_ptr, rec_image);
		// to cv image
	 	cv_bridge::CvImagePtr cv_ptr;
	 	cv_ptr = cv_bridge::toCvCopy(rec_image, sensor_msgs::image_encodings::BGR8);
	 	/*cv::Point p(srv.response.fourPoints[0],srv.response.fourPoints[1]);
	 	cv::circle( cv_ptr->image, p, 2.0, cv::Scalar( 255, 0, 0 ), -1, 8);
	 	cv::Point p1(srv.response.fourPoints[2],srv.response.fourPoints[3]);
	 	cv::circle( cv_ptr->image, p1, 2.0, cv::Scalar( 0, 255, 0 ), -1, 8);
	 	cv::Point p2(srv.response.fourPoints[4],srv.response.fourPoints[5]);
	 	cv::circle( cv_ptr->image, p2, 2.0, cv::Scalar( 0, 0, 255 ), -1, 8);
	 	cv::Point p3(srv.response.fourPoints[6],srv.response.fourPoints[7]);
	 	cv::circle( cv_ptr->image, p3, 2.0, cv::Scalar( 105, 0, 205 ), -1, 8);*/
		cv::imshow("view", cv_ptr->image);
		cv::waitKey();
		
		// show one by one
		int total = srv.response.boxes.size();
		for(int j=0; j<total; ++j){
			float max = FLT_MIN;
			int idx;
			sensor_msgs::ImagePtr im;
			for (int k=0; k<srv.response.boxes.size();++k){
				if(max < srv.response.boxes[k].score){
					idx = k;
					max = srv.response.boxes[k].score;
				}
			}
			ROS_INFO("#%d, index: %d score: %f", j, idx, max);
			deep_rec::BoxMsg b = srv.response.boxes[idx];
			
			ROS_INFO("box -- x1: %f, y1: %f, x2: %f, y2: %f, c2t: %f, s2t: %f ",b.box[0], b.box[1], b.box[2],
								b.box[3], b.box[4], b.box[5]);
			
			TwoPointRectangle t(b.box[0], b.box[1], b.box[2], 
									b.box[3], b.box[4], b.box[5]);
			std::vector<TwoPointRectangle> b_vec;
			b_vec.push_back(t);
			DataCollector::get_rectangle_image(b_vec, *img_ptr, im);
			cv_bridge::CvImagePtr im_ptr;
	 		im_ptr = cv_bridge::toCvCopy(im, sensor_msgs::image_encodings::BGR8);
			cv::imshow("box only", im_ptr->image);
			cv::waitKey();
			//remove
			srv.response.boxes.erase(srv.response.boxes.begin()+idx);
		}
		
	}
	else
	{
		ROS_ERROR("Failed to call service");
		return 1;
	}

	return 0;
}
