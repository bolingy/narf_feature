#include <depth_feature/depth_feature.hpp>

/*
	
	DEBUG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
*/
//int bi = 1;
//int ei = bi+1;


int filter_table(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud){

	std::cerr << "Point cloud data: " << cloud->points.size () << " points" << std::endl;
	for (size_t i = 0; i < cloud->points.size (); ++i)
	std::cerr << "    " << cloud->points[i].x << " "
		                << cloud->points[i].y << " "
		                << cloud->points[i].z << std::endl;

	pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
	// Create the segmentation object
	pcl::SACSegmentation<pcl::PointXYZ> seg;
	// Optional
	seg.setOptimizeCoefficients (true);
	// Mandatory
	seg.setModelType (pcl::SACMODEL_PLANE);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setDistanceThreshold (0.01);

	seg.setInputCloud (cloud);
	seg.segment (*inliers, *coefficients);

	if (inliers->indices.size () == 0)
	{
	PCL_ERROR ("Could not estimate a planar model for the given dataset.");
	return (-1);
	}

	std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
		                              << coefficients->values[1] << " "
		                              << coefficients->values[2] << " " 
		                              << coefficients->values[3] << std::endl;

	std::cerr << "Model inliers: " << inliers->indices.size () << std::endl;
	for (size_t i = 0; i < inliers->indices.size (); ++i)
	std::cerr << inliers->indices[i] << "    " << cloud->points[inliers->indices[i]].x << " "
		                                       << cloud->points[inliers->indices[i]].y << " "
		                                       << cloud->points[inliers->indices[i]].z << std::endl;

	return (0);
}

void setViewerPose (pcl::visualization::PCLVisualizer& viewer, const Eigen::Affine3f& viewer_pose)
{
  Eigen::Vector3f pos_vector = viewer_pose * Eigen::Vector3f (0, 0, 0);
  Eigen::Vector3f look_at_vector = viewer_pose.rotation () * Eigen::Vector3f (0, 0, 1) + pos_vector;
  Eigen::Vector3f up_vector = viewer_pose.rotation () * Eigen::Vector3f (0, -1, 0);
  viewer.setCameraPosition (pos_vector[0], pos_vector[1], pos_vector[2],
                            look_at_vector[0], look_at_vector[1], look_at_vector[2],
                            up_vector[0], up_vector[1], up_vector[2]);
}

void closestPoints(const pcl::PointCloud<PointType>& input_cloud,
					pcl::PointCloud<PointType>& output_cloud,
					pcl::PointXYZ keypoint,
					float distance){
	ROS_INFO("kx %f, ky %f", keypoint.x, keypoint.x);
	float x2_dis = distance/2 + keypoint.x;
	float y2_dis = distance/2 + keypoint.y;
	float x1_dis = keypoint.x - distance/2;
	float y1_dis = keypoint.y - distance/2;
	pcl::PointXYZ point1,point2;
	float minDis1=FLT_MAX, minDis2=FLT_MAX;
	// iterate through all points
	for(unsigned int i=0; i<input_cloud.points.size(); i++){
		pcl::PointXYZ cur_point = input_cloud.points[i];
		// check distance
		float cur_dis1 = sqrt(pow(fabs(cur_point.x-x1_dis),2) + pow(fabs(cur_point.y-y1_dis),2));
		float cur_dis2 = sqrt(pow(fabs(cur_point.x-x2_dis),2) + pow(fabs(cur_point.y-y2_dis),2));
		if (cur_dis1 < minDis1){
			point1 = cur_point;
			minDis1 = cur_dis1;
		}
		if (cur_dis2 < minDis2){
			point2 = cur_point;
			minDis2 = cur_dis2;
		}
	}
	// add points to output
	const pcl::PointXYZ p1 = point1;
	const pcl::PointXYZ p2 = point2;
	output_cloud.push_back(p1);
	output_cloud.push_back(p2);
	ROS_ERROR("minDis1 : %f, minDis2: %f",minDis1, minDis2 );
}

void sparsePoint(const pcl::PointCloud<PointType>& input_cloud,
					pcl::PointCloud<pcl::PointXYZ>& keypoints,
					float distance,
					const std::vector<int>& desp_dir,
					pcl::PointCloud<PointType>& result_cloud){
	
	// for each keypoint
	for (int i=0;i<keypoints.points.size();++i){
		// current point
		pcl::PointXYZ key_point = keypoints.points[i];
		//ROS_INFO("keypoint x:%f, y: %f", key_point.x, key_point.y);
		// for all three of the dominant beams
		for (int di=3*i;di<3*i+3;++di){
			// check degree
			float radian = 10 * desp_dir[di] * M_PI / 180;
			// get world coordinate
			float wld_x = key_point.x + distance * sin(radian);
			float wld_y = key_point.y - distance * cos(radian);
			//ROS_INFO("x: %f, y: %f, with direction: %d distance: %f", wld_x, wld_y, desp_dir[di], distance);
			
			pcl::PointXYZ result_point;
			float minDis=FLT_MAX;
			// iterate throught all points
			for (unsigned int j = 0; j<input_cloud.points.size(); ++j){
				pcl::PointXYZ in_point = input_cloud.points[j];
				// check distance
				float cur_dis = sqrt(pow(in_point.x-wld_x,2) + pow(in_point.y-wld_y,2));
				//ROS_INFO("in point x:%f ,y:%f ,cur_dis: %f",in_point.x,in_point.y, cur_dis);
				if(isnan(in_point.x)||isnan(in_point.y)||isnan(in_point.z)) continue;
				
				if (cur_dis < minDis){
					//ROS_INFO("updated result x:%f ,y:%f",in_point.x,in_point.y);
					result_point = in_point;
					minDis = cur_dis;
				}
			}
			result_cloud.push_back(result_point);
		}
	}
	//ROS_WARN("returning from sparse point");
}

// find the two orthogonal descriptors
void orthPoint(const pcl::PointCloud<PointType>& input_cloud,
					pcl::PointCloud<pcl::PointXYZ>& keypoints,
					const pcl::PointCloud<pcl::Narf36>& narf_descriptors,
					float distance,
					const std::vector<int>& desp_dir,
					pcl::PointCloud<PointType>& orth_cloud,
					pcl::PointCloud<PointType>& diag_cloud,
					std::vector<float>& rec_score
				){
	// for all keypoints
	for (int i=0;i<keypoints.points.size();++i){
		for (int di=3*i;di<3*i+3;++di){
			//// get larger orth
			int vector_1 = desp_dir[di] + 9;
			int vector_2 = desp_dir[di] + 27;
			if (vector_1>=36) vector_1 = vector_1-36;
			if (vector_2>=36) vector_2 = vector_2-36;
			
			/*************************************************************************************************************
			
				Debug!!!!!!!
				change back to narf_descriptors.points[i]  !!	***************************************************************************************************************/
			
			
			//ROS_INFO("vector 1: %d = %f, 2: %d = %f", vector_1, narf_descriptors.points[i].descriptor[vector_1], vector_2, narf_descriptors.points[i].descriptor[vector_2]);
			//
			int better_dir;
			float temp;
			//ROS_INFO("before narf descrip");
			if(fabs(narf_descriptors.points[i].descriptor[vector_1]) >= 
					fabs(narf_descriptors.points[i].descriptor[vector_2])){
				better_dir = vector_1;
				temp = 0 - (45 * M_PI/180);
			}else{
				better_dir = vector_2;
				temp = (45 * M_PI/180);
			}
			
			
			rec_score[di] += fabs(narf_descriptors.points[i].descriptor[better_dir]);
			float angle = 10 * better_dir * M_PI/180;
			float diag_angle = angle + temp;
		
			// get world coordinate
			float wld_x = keypoints.points[i].x + distance * sin(angle);
			float wld_y = keypoints.points[i].y - distance * cos(angle);
			float diag_x = keypoints.points[i].x + distance * sqrt(2) * sin(diag_angle);
			float diag_y = keypoints.points[i].y - distance * sqrt(2) * cos(diag_angle);
			pcl::PointXYZ orth_point, diag_point;
			float minDis = FLT_MAX;
			float minDiagDis = FLT_MAX;
			// iterate throught all points
			for (unsigned int j = 0; j<input_cloud.points.size(); ++j){
				pcl::PointXYZ in_point = input_cloud.points[j];
				// check distance
				float cur_dis = sqrt(pow(in_point.x-wld_x,2) + pow(in_point.y-wld_y,2));
				float cur_diag_dis = sqrt(pow(in_point.x-diag_x,2) + pow(in_point.y-diag_y,2));
				// update orth
				if (cur_dis < minDis){
					orth_point = in_point;
					minDis = cur_dis;
				}
				// update diag
				if (cur_diag_dis < minDiagDis){
					diag_point = in_point;
					minDiagDis = cur_diag_dis;
				}
			}
			//ROS_WARN("find points: %f, %f, %f, %f", orth_point.x, orth_point.y, diag_point.x, diag_point.y);
			orth_cloud.push_back(orth_point);
			diag_cloud.push_back(diag_point);
		}
	}
}

void getCenterPix(vector<float>& xy, float& cx, float& cy){
	float x1 = xy[0], y1 = xy[1], x2 = xy[2], y2 = xy[3], x3 = xy[4], y3 = xy[5],
			x4 = xy[6], y4 = xy[7];
	float k1 = (y2 - y3)/(x2 - x3);
	float k2 = (y4 - y1)/(x4 - x1);
	float b1 = y2 - k1*x2;
	float b2 = y4 - k2*x4;
	cx = (b2-b1)/(k1-k2);
	cy = (k1*cx + b1);
}

void rotatePoint(float cx, float cy, float angle, float& x, float& y){
	float s = sin(angle);
  	float c = cos(angle);

  	// translate point back to origin:
	x -= cx;
	y -= cy;

	// rotate point
	float xnew = x * c - y * s;
	float ynew = x * s + y * c;

	// translate point back:
	x = xnew + cx;
	y = ynew + cy;
	
	//ROS_WARN("angle: %f, xnew: %f, ynew: %f, cx: %f, cy: %f", angle, xnew, ynew, cx, cy);
}


TwoPointRectangle pixTwoPointsRec( vector<float>& xy){
	float x1 = xy[0], y1 = xy[1], x2 = xy[2], y2 = xy[3], x3 = xy[4], y3 = xy[5],
			x4 = xy[6], y4 = xy[7];
	// find rotational angle
	float angle1 = atan((y2-y1)/(x2-x1));
	float angle2 = atan((y4-y3)/(x4-x3));
	float rotation = (angle1 + angle2)/2;
	// find center
	float cx, cy;
	getCenterPix(xy, cx, cy);
	rotatePoint(cx, cy, -rotation, x1, y1);
	rotatePoint(cx, cy, -rotation, x4, y4);
	TwoPointRectangle rec(x1, y1, x4, y4, cos(2*rotation), sin(2*rotation));
	return rec;
}



//
void cloudForPix(const pcl::PointCloud<PointType>& result_cloud, 
					const pcl::PointCloud<PointType>& orth_cloud,
					 std::vector< pcl::PointCloud<PointType> >& pix_clouds){
	// for each point in orth and result cloud
	for (int i=0;i<result_cloud.points.size();++i){
		pcl::PointCloud<PointType> pix_cloud;
		PointType pix_point_1, pix_point_2;
		//ROS_INFO("x1:%f, y1:%f, z1:%f, x2:%f, y2:%f, z2:%f",result_cloud.points[i].x, result_cloud.points[i].y,
		//		result_cloud.points[i].z,orth_cloud.points[i].x,orth_cloud.points[i].y,orth_cloud.points[i].z);
		pix_point_1 = result_cloud.points[i];
		pix_point_2 = orth_cloud.points[i];
		pix_cloud.points.push_back(pix_point_1);
		pix_cloud.points.push_back(pix_point_2);
		pix_clouds.push_back(pix_cloud);
	}
}

void point2Pix(const sensor_msgs::CameraInfo& camera_info,
				pcl::PointCloud<PointType> output_cloud,
				vector<double>& pix){
	Eigen::Vector3d p1_camera_link;
    Eigen::Vector3d p2_camera_link;
    Eigen::Vector3d p1_screen;
    Eigen::Vector3d p2_screen;
    Eigen::Matrix3d link_to_screen;
    
    link_to_screen << camera_info.K[0], camera_info.K[1], camera_info.K[2],
                      camera_info.K[3], camera_info.K[4], camera_info.K[5],
                      camera_info.K[6], camera_info.K[7], camera_info.K[8];
                     
    double x1 = 0.0;
    double y1 = 0.0;
    double x2 = 0.0;
    double y2 = 0.0;
    double angle = 0.0;
	
	//ROS_INFO("x1:%f, y1:%f, z1:%f, x2:%f, y2:%f, z2:%f",output_cloud.points[0].x, output_cloud.points[0].y,
	//			output_cloud.points[0].z,output_cloud.points[1].x,output_cloud.points[1].y,output_cloud.points[1].z);
	
	p1_camera_link(0) = output_cloud.points[0].x;
    p1_camera_link(1) = output_cloud.points[0].y;
    p1_camera_link(2) = output_cloud.points[0].z;
    p2_camera_link(0) = output_cloud.points[1].x;
    p2_camera_link(1) = output_cloud.points[1].y;
    p2_camera_link(2) = output_cloud.points[1].z;
    
    
    p1_screen = link_to_screen*p1_camera_link;
    p2_screen = link_to_screen*p2_camera_link;
    
    x1 = std::min(p1_screen(0)/p1_screen(2), p2_screen(0)/p2_screen(2));
    y1 = std::min(p1_screen(1)/p1_screen(2), p2_screen(1)/p2_screen(2));
    x2 = std::max(p1_screen(0)/p1_screen(2), p2_screen(0)/p2_screen(2));
    y2 = std::max(p1_screen(1)/p1_screen(2), p2_screen(1)/p2_screen(2));
    pix.push_back(x1);
    pix.push_back(y1);
    pix.push_back(x2);
    pix.push_back(y2);
    pix.push_back(1);
    pix.push_back(0);
}

void onePoint2Pix(const sensor_msgs::CameraInfo& camera_info,
				PointType& point,
				vector<double>& pix){
	Eigen::Vector3d camera_link, screen;
	Eigen::Matrix3d link_to_screen;
	
	link_to_screen << camera_info.K[0], camera_info.K[1], camera_info.K[2],
                      camera_info.K[3], camera_info.K[4], camera_info.K[5],
                      camera_info.K[6], camera_info.K[7], camera_info.K[8];
                  
	double x = 0.0;
    double y = 0.0;
    double angle = 0.0;
    
    camera_link(0) = point.x;
    camera_link(1) = point.y;
    camera_link(2) = point.z;
    
    screen = link_to_screen*camera_link;
    
    x = screen(0)/screen(2);
    y = screen(1)/screen(2);
    pix.push_back(x);
    pix.push_back(y);
}

int getMinIndex(vector<double>& temp){
	vector<double>::iterator result = min_element(temp.begin(), temp.end());
	return distance(temp.begin(), result);
}

int getMaxIndex(vector<float>& temp){
	vector<float>::iterator result = max_element(temp.begin(), temp.end());
	return distance(temp.begin(), result);
}

void getOrder(const vector<double>& ins_pix,
				const vector<double>& res_pix,
				const vector<double>& orth_pix, 
				const vector<double>& diag_pix,
				vector<float>& xy){
	float x1, y1, x2, y2, x3, y3, x4, y4;
	const double arrx[] = {ins_pix[0], res_pix[0], orth_pix[0], diag_pix[0]};
	const double arry[] = {ins_pix[1], res_pix[1], orth_pix[1], diag_pix[1]};
	const vector<double> xlist (arrx, arrx + sizeof(arrx)/sizeof(arrx[0]));
	const vector<double> ylist (arry, arry + sizeof(arry)/sizeof(arry[0]));
	// point 1 and 3
	vector<double> temp = xlist;
	int idx1 = getMinIndex(temp);
	temp[idx1] = DBL_MAX;
	int idx2 = getMinIndex(temp);
	//ROS_WARN("all pixs:  %f, %f, %f, %f, %f, %f, %f, %f", 
		//(float)xlist[0], (float)ylist[0], (float)xlist[1], (float)ylist[1], (float)xlist[2], (float)ylist[2], (float)xlist[3], (float)ylist[3]);
	
	if (ylist[idx1]<ylist[idx2]){
		x1 = (float)xlist[idx1];
		y1 = (float)ylist[idx1];
		x3 = (float)xlist[idx2];
		y3 = (float)ylist[idx2];
	}else{
		x1 = (float)xlist[idx2];
		y1 = (float)ylist[idx2];
		x3 = (float)xlist[idx1];
		y3 = (float)ylist[idx1];
	}
	
	// point 2 and 4
	int idx3 = 8, idx4 = 8;
	for (int i=0; i<4; ++i){
		if (i != idx1 && i!= idx2){
			if (idx3 == 8) idx3 = i;
			else idx4 = i;
		}
	}
	
	if (ylist[idx3]<ylist[idx4]){
		x2 = (float)xlist[idx3];
		y2 = (float)ylist[idx3];
		x4 = (float)xlist[idx4];
		y4 = (float)ylist[idx4];		
	}else{
		x2 = (float)xlist[idx4];
		y2 = (float)ylist[idx4];
		x4 = (float)xlist[idx3];
		y4 = (float)ylist[idx3];
	}
	//ROS_WARN("after: %f, %f, %f, %f, %f, %f, %f, %f", x1, y1, x2, y2, x3, y3, x4, y4);
	const float arr[] = {x1, y1, x2, y2, x3, y3, x4, y4};
	vector<float> result(arr, arr + sizeof(arr)/sizeof(arr[0]));
	xy = result;
}

bool check_point_valid(deep_rec::BoxMsg box_msg){
	for(int i=0; i<box_msg.box.size(); ++i){
		if (isnan(box_msg.box[i])!=0)
			return false;
	}
	return true;
}


bool narf_rect_detection(depth_feature::narf_rect::Request &req,
						depth_feature::narf_rect::Response &res){
	
	// set variables
	pcl::PointCloud<PointType>::Ptr point_cloud_ptr (new pcl::PointCloud<PointType>);
	pcl::PointCloud<PointType>& point_cloud = *point_cloud_ptr;
	Eigen::Affine3f scene_sensor_pose (Eigen::Affine3f::Identity ());
	float support_size = 2*sqrt(req.object_size*0.1/M_PI);
	//float support_size = 0.1f;

	// convert sensor_message pc to baselink then to pcl
    //sensor_msgs::PointCloud2 base_cloud;
 	//const tf::TransformListener tf_listener;
    //pcl_ros::transformPointCloud ("base_link", req.input_cloud, base_cloud,  tf_listener);
 
    pcl::PCLPointCloud2 pcl_pc2;
	pcl_conversions::toPCL(req.input_cloud, pcl_pc2);
	pcl::fromPCLPointCloud2(pcl_pc2, *point_cloud_ptr);
   	
   	
   	// receive transform
 	/*tf::TransformListener tf_listener;
 	geometry_msgs::PoseStamped pin;
  	pin.header.frame_id = "head_mount_kinect_rgb_optical_frame";
 	pin.header.seq = 0;
  	pin.header.stamp = ros::Time(0);
  	// input target pose for gripper
	pin.pose.position.x = 0.0;
	pin.pose.position.y = 0.0;
	pin.pose.position.z = 0.0;
	pin.pose.orientation.x = 0;
	pin.pose.orientation.y = 0;
	pin.pose.orientation.z = 0;
	pin.pose.orientation.w = 1;
	// transform
	geometry_msgs::PoseStamped pout;
	tf_listener.waitForTransform("base_link", "head_mount_kinect_rgb_optical_frame", ros::Time::now(), ros::Duration(1));
	tf_listener.transformPose("base_link", pin, pout);
	// convert camera info to sensor pose
   	const geometry_msgs::PoseStamped msg = pout;
   	Eigen::Affine3d eigen_out;
   	tf::poseMsgToEigen(msg.pose, eigen_out);
    Eigen::Affine3f scene_sensor_pose = eigen_out.cast<float>();*/
   	
   	
   	// set camera pose
    scene_sensor_pose = Eigen::Affine3f (Eigen::Translation3f (point_cloud.sensor_origin_[0],
                                                               point_cloud.sensor_origin_[1],
                                                               point_cloud.sensor_origin_[2])) *
                        Eigen::Affine3f (point_cloud.sensor_orientation_);
                        
  
	// Create RangeImage from the PointCloud
	float noise_level = 0.0;
	float min_range = 0.0f;
	int border_size = 1;
	boost::shared_ptr<pcl::RangeImage> range_image_ptr (new pcl::RangeImage);
	pcl::RangeImage& range_image = *range_image_ptr;
	range_image.createFromPointCloud (point_cloud, angularResolution, maxAngleWidth, maxAngleHeight,
		                           scene_sensor_pose, coordinate_frame, noise_level, min_range, border_size);
	
	
	if (setUnseenToMaxRange)
	range_image.setUnseenToMaxRange ();

	// Open 3D viewer and add point cloud
	pcl::visualization::PCLVisualizer viewer ("3D Viewer");
	viewer.setBackgroundColor (1, 1, 1);
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> range_image_color_handler (range_image_ptr, 0, 0, 0);
	viewer.addPointCloud (range_image_ptr, range_image_color_handler, "range image");
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "range image");
	viewer.initCameraParameters ();
	setViewerPose (viewer, range_image.getTransformationToWorldSystem ());
	
	// Show range image
	pcl::visualization::RangeImageVisualizer range_image_widget ("Range image");
	range_image_widget.showRangeImage (range_image);

	// Extract NARF keypoints
	pcl::RangeImageBorderExtractor range_image_border_extractor;
	pcl::NarfKeypoint narf_keypoint_detector;
	narf_keypoint_detector.setRangeImageBorderExtractor (&range_image_border_extractor);
	narf_keypoint_detector.setRangeImage (&range_image);
	narf_keypoint_detector.getParameters ().support_size = support_size;

	pcl::PointCloud<int> keypoint_indices;
	narf_keypoint_detector.compute (keypoint_indices);
	std::cout << "Found "<<keypoint_indices.points.size ()<<" key points.\n";

	// -----Show keypoints in range image widget-----
	//for (size_t i=0; i<keypoint_indices.points.size (); ++i)
	//range_image_widget.markPoint (keypoint_indices.points[i]%range_image.width,
		                          //keypoint_indices.points[i]/range_image.width);


	// Extract NARF descriptors for interest points
	std::vector<int> keypoint_indices2;
	keypoint_indices2.resize (keypoint_indices.points.size ());
	for (unsigned int i=0; i<keypoint_indices.size (); ++i) // This step is necessary to get the right vector type
	keypoint_indices2[i]=keypoint_indices.points[i];
	pcl::NarfDescriptor narf_descriptor (&range_image, &keypoint_indices2);
	narf_descriptor.getParameters ().support_size = support_size;
	narf_descriptor.getParameters ().rotation_invariant = rotation_invariant;
	pcl::PointCloud<pcl::Narf36> narf_descriptors;
	narf_descriptor.compute (narf_descriptors);
	cout << "Extracted "<<narf_descriptors.size ()<<" descriptors for "
		              <<keypoint_indices.points.size ()<< " keypoints.\n";
		              
	// Find dominate point
	float mx_change = FLT_MIN;
	int index;
	for (int i=0; i<narf_descriptors.size (); ++i){
		float temp_sum = 0;
		for (int j=0; j<36;j++){
			temp_sum += fabs(narf_descriptors.points[i].descriptor[j]);
			//cout << "before: " <<narf_descriptors.points[i].descriptor[j]<< " after "
			//<< fabs(narf_descriptors.points[i].descriptor[j]) << "\n";
		}
		//cout << "point: " << temp_sum << "\n";
		if(temp_sum > mx_change){
			mx_change = temp_sum;
			index = i;
		}
	}
	
	ROS_INFO("found dominate");
	
	
	// Show keypoints in 3D viewer
	pcl::PointCloud<pcl::PointXYZ>::Ptr keypoints_ptr (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>& keypoints = *keypoints_ptr;
	keypoints.points.resize (keypoint_indices.points.size ());
	for (size_t i=0; i<keypoint_indices.points.size (); ++i)
	keypoints.points[i].getVector3fMap () = range_image.points[keypoint_indices.points[i]].getVector3fMap ();
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> keypoints_color_handler (keypoints_ptr, 0, 255, 0);
	viewer.addPointCloud<pcl::PointXYZ> (keypoints_ptr, keypoints_color_handler, "keypoints");
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "keypoints");

	// Re-plot dominate point
	pcl::PointCloud<pcl::PointXYZ>::Ptr fin_keypoints_ptr (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>& fin_keypoints = *fin_keypoints_ptr;
	fin_keypoints.points.resize (1);
	fin_keypoints.points[0].getVector3fMap () = range_image.points[keypoint_indices.points[index]].getVector3fMap ();
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> fin_keypoints_color_handler (fin_keypoints_ptr, 255, 0, 0);
	viewer.addPointCloud<pcl::PointXYZ> (fin_keypoints_ptr, fin_keypoints_color_handler, "fin_keypoints");
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "fin_keypoints");


	// create rectangle
	pcl::PointCloud<pcl::PointXYZ>::Ptr output_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
	const pcl::PointCloud<PointType> input_cloud  =  *point_cloud_ptr;
	pcl::PointCloud<PointType>& output_cloud = *output_cloud_ptr;
	closestPoints(input_cloud, output_cloud, fin_keypoints.points[0], support_size);
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> rec_color_handler (output_cloud_ptr, 0, 0, 255);
	viewer.addPointCloud<pcl::PointXYZ> (output_cloud_ptr, rec_color_handler, "rec_keypoints");
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "rec_keypoints");
	
	
	const sensor_msgs::CameraInfo camera_info = req.camera_info;
	vector<double> box;
	point2Pix(camera_info, output_cloud, box);
	deep_rec::BoxMsg b0;
	b0.box[0]=box[0], b0.box[1]=box[1], b0.box[2]=box[2], b0.box[3]=box[3], b0.box[4]=box[4], b0.box[5]=box[5];
	b0.score = 1;
	res.boxes.push_back(b0);
	
	
	//--------------------
	// -----Main loop-----
	//--------------------
	while (!viewer.wasStopped ())
	{
	range_image_widget.spinOnce ();  // process GUI events
	viewer.spinOnce ();
	pcl_sleep(0.01);
	}
	
	return true;
}

bool check_box_exist(deep_rec::BoxMsg msg, vector<deep_rec::BoxMsg> vec){
	
	for(int i = 0;i<vec.size();++i){
		deep_rec::BoxMsg cur = vec[i];
		if(msg.box[0]==cur.box[0] && msg.box[1]==cur.box[1] && msg.box[2]==cur.box[2] &&
			msg.box[3]==cur.box[3] && msg.box[4]==cur.box[4] && msg.box[5]==cur.box[5])
			return true;
	}
	return false;
}
 
void filter_rangePoint(pcl::PointCloud<pcl::PointXYZ>& kp,
						pcl::PointCloud<int>& keypoint_indices,
						pcl::RangeImage& range_image){

	float maxX = FLT_MIN;
	float minX = FLT_MAX;
	float maxY = FLT_MIN;
	float minY = FLT_MAX;
	float thre = 0.1;
	// find max min x, y
	for (size_t i=0; i<range_image.points.size(); ++i){
		pcl::PointXYZ temp;
		temp.getVector3fMap () = range_image.points[i].getVector3fMap ();
		if(maxX < temp.x)
			maxX = temp.x;
		if(minX > temp.x)
			minX = temp.x;
		if(maxY < temp.y)
			maxY = temp.y;
		if(minY > temp.y)
			minY = temp.y;
	}
	// remove from keypoint_indices
	for(int j=keypoint_indices.points.size()-1;j>=0;--j){
		pcl::PointXYZ temp_key;
		temp_key.getVector3fMap () = range_image.points[keypoint_indices.points[j]].getVector3fMap ();
		if(temp_key.x - minX < thre || maxX - temp_key.x < thre ||
			temp_key.y - minY < thre || maxY - temp_key.y < thre) {
			keypoint_indices.points.erase(keypoint_indices.points.begin() + j);
		}
	}
	// 
	kp.points.resize (keypoint_indices.points.size());
	for(int k=0; k<keypoint_indices.points.size();++k)
		kp.points[k].getVector3fMap () = range_image.points[keypoint_indices.points[k]].getVector3fMap ();
	
}

bool narf_sparse_detection(depth_feature::narf_rect::Request &req,
						depth_feature::narf_rect::Response &res){
						
	// set variables
	pcl::PointCloud<PointType>::Ptr point_cloud_ptr (new pcl::PointCloud<PointType>);
	pcl::PointCloud<PointType>& point_cloud = *point_cloud_ptr;
	Eigen::Affine3f scene_sensor_pose (Eigen::Affine3f::Identity ());
	float support_size = 2*sqrt(req.object_size*0.50);
	//float support_size = 0.1f;

	pcl::PCLPointCloud2 pcl_pc2;
	pcl_conversions::toPCL(req.input_cloud, pcl_pc2);
	pcl::fromPCLPointCloud2(pcl_pc2, *point_cloud_ptr);
   	
   	// set camera pose
    scene_sensor_pose = Eigen::Affine3f (Eigen::Translation3f (point_cloud.sensor_origin_[0],
                                                               point_cloud.sensor_origin_[1],
                                                               point_cloud.sensor_origin_[2])) *
                        Eigen::Affine3f (point_cloud.sensor_orientation_);
                        
  
	// Create RangeImage from the PointCloud
	float noise_level = 0.0;
	float min_range = 0.0f;
	int border_size = 1;
	boost::shared_ptr<pcl::RangeImage> range_image_ptr (new pcl::RangeImage);
	pcl::RangeImage& range_image = *range_image_ptr;
	range_image.createFromPointCloud (point_cloud, angularResolution, maxAngleWidth, maxAngleHeight,
		                           scene_sensor_pose, coordinate_frame, noise_level, min_range, border_size);
	
	if (setUnseenToMaxRange)
	range_image.setUnseenToMaxRange ();

	// Open 3D viewer and add point cloud
	/*pcl::visualization::PCLVisualizer viewer ("3D Viewer");
	viewer.setBackgroundColor (1, 1, 1);
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointWithRange> range_image_color_handler (range_image_ptr, 0, 0, 0);
	viewer.addPointCloud (range_image_ptr, range_image_color_handler, "range image");
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "range image");
	viewer.initCameraParameters ();
	setViewerPose (viewer, range_image.getTransformationToWorldSystem ());
	
	// Show range image
	pcl::visualization::RangeImageVisualizer range_image_widget ("Range image");
	range_image_widget.showRangeImage (range_image);*/

	// Extract NARF keypoints
	pcl::RangeImageBorderExtractor range_image_border_extractor;
	pcl::NarfKeypoint narf_keypoint_detector;
	narf_keypoint_detector.setRangeImageBorderExtractor (&range_image_border_extractor);
	narf_keypoint_detector.setRangeImage (&range_image);
	narf_keypoint_detector.getParameters ().support_size = support_size;
	pcl::PointCloud<int> keypoint_indices;
	narf_keypoint_detector.compute (keypoint_indices);

	ROS_INFO("NARF keypoint found.");
	// filter bad keypoints
	pcl::PointCloud<pcl::PointXYZ>::Ptr keypoints_ptr (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>& keypoints = *keypoints_ptr;
	filter_rangePoint(keypoints, keypoint_indices, range_image);


	// Extract NARF descriptors for interest points
	std::vector<int> keypoint_indices2;
	keypoint_indices2.resize (keypoint_indices.points.size ());
	for (unsigned int i=0; i<keypoint_indices.size (); ++i) // This step is necessary to get the right vector type
	keypoint_indices2[i]=keypoint_indices.points[i];
	pcl::NarfDescriptor narf_descriptor (&range_image, &keypoint_indices2);
	narf_descriptor.getParameters ().support_size = support_size;
	narf_descriptor.getParameters ().rotation_invariant = rotation_invariant;
	pcl::PointCloud<pcl::Narf36> narf_descriptors;
	narf_descriptor.compute (narf_descriptors);
	//cout << "Extracted "<<narf_descriptors.size ()<<" descriptors for "
	//	              <<keypoint_indices.points.size ()<< " keypoints.\n";
		              
	ROS_INFO("NARF descriptor found, finding recs.");
	// for each interest point find 3 dominant beams
	std::vector<int> desp_dir;
	std::vector<float> rec_score;
	
	
	/**********************************************************************************************
		
		Change loop to debug
		//for (int i=bi; i<ei; ++i){
	**********************************************************************************************/
	
	
	
	
	for (int i=0; i<narf_descriptors.points.size(); ++i){
		vector<float> beams;
		// descriptor to vector
		for (int j=1; j<36;j++){
			beams.push_back(fabs(narf_descriptors.points[i].descriptor[j]));
		}
		// get the largest three beams
		// record score for 
		int dir_1 = getMaxIndex(beams);
		rec_score.push_back(beams[dir_1]);
		beams[dir_1] = FLT_MIN;
		int dir_2 = getMaxIndex(beams);
		rec_score.push_back(beams[dir_2]);
		beams[dir_2] = FLT_MIN;
		int dir_3 = getMaxIndex(beams);
		rec_score.push_back(beams[dir_3]);
		ROS_INFO("dominant directions: %d, %d, %d",dir_1,dir_2,dir_3);
		// store direction for current descriptor
		desp_dir.push_back(dir_1);
		desp_dir.push_back(dir_2);
		desp_dir.push_back(dir_3);
	}
	
	// Show keypoints in 3D viewer
	
	
	/********************************************************************************************
			Change keypoint size to debug:
	keypoints.points.resize (1);
	for (size_t i=bi; i<ei; ++i){
		keypoints.points[0].getVector3fMap () = range_image.points[keypoint_indices.points[i]].getVector3fMap ();
	*********************************************************************************************/
	
	for (size_t i=0; i<keypoint_indices.points.size (); ++i){
		keypoints.points[i].getVector3fMap () = range_image.points[keypoint_indices.points[i]].getVector3fMap ();
	}
	/*pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> keypoints_color_handler (keypoints_ptr, 0, 255, 0);
	viewer.addPointCloud<pcl::PointXYZ> (keypoints_ptr, keypoints_color_handler, "keypoints");
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "keypoints");*/
	
	// Find dominant directions
	pcl::PointCloud<pcl::PointXYZ>::Ptr result_cloud_ptr (new pcl::PointCloud<pcl::PointXYZ>);
	const pcl::PointCloud<PointType> input_cloud  =  *point_cloud_ptr;
	pcl::PointCloud<PointType>& result_cloud = *result_cloud_ptr;
	sparsePoint(input_cloud, keypoints, support_size/2, desp_dir, result_cloud);
	
	// find orthogonal edge and diag point
	pcl::PointCloud<pcl::PointXYZ>::Ptr orth_ptr (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr diag_ptr (new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<PointType>& orth_cloud = *orth_ptr;
	pcl::PointCloud<PointType>& diag_cloud = *diag_ptr;
	orthPoint(input_cloud, keypoints, narf_descriptors, support_size/2, desp_dir, orth_cloud, diag_cloud, rec_score);
	//// draw points
	/*pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> rec_color_handler (result_cloud_ptr, 0, 0, 255);
	viewer.addPointCloud<pcl::PointXYZ> (result_cloud_ptr, rec_color_handler, "rec_keypoints");
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "rec_keypoints");
	// orth
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> orth_color_handler (orth_ptr, 255, 0, 0);
	viewer.addPointCloud<pcl::PointXYZ> (orth_ptr, orth_color_handler, "orth_keypoints");
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "orth_keypoints");
	// diag
	pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> diag_color_handler (diag_ptr, 102, 0, 205);
	viewer.addPointCloud<pcl::PointXYZ> (diag_ptr, diag_color_handler, "diag_keypoints");
	viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 7, "diag_keypoints");*/
	
	//// create boxes
	const sensor_msgs::CameraInfo camera_info = req.camera_info;
	//std::vector< pcl::PointCloud<PointType> > pix_clouds;
	//cloudForPix(result_cloud, orth_cloud, pix_clouds);
	
	// find max score
	float max_score = rec_score[getMaxIndex(rec_score)];
	vector<deep_rec::BoxMsg> temp_list;
	// for each interest point
	for (int i=0; i<keypoints.points.size(); ++i){
		for(int di=3*i;di<3*i+3;++di){
			vector<double> ins_pix, res_pix, orth_pix, diag_pix;
			// box 
			onePoint2Pix(camera_info, keypoints.points[i], ins_pix);
			onePoint2Pix(camera_info, result_cloud.points[di], res_pix);
			onePoint2Pix(camera_info, orth_cloud.points[di], orth_pix);
			onePoint2Pix(camera_info, diag_cloud.points[di], diag_pix);
			// determine p1 p2 p3 p4
			vector<float> rec_xy; // [x1,y1,x2,y2,x3,y3,x4,y4]
			getOrder(ins_pix, res_pix, orth_pix, diag_pix, rec_xy);
			// calculate two point rec in pixel level
			TwoPointRectangle tRec = pixTwoPointsRec(rec_xy);
			deep_rec::BoxMsg box_msg;
			box_msg.box[0]=tRec.x1(), box_msg.box[1]=tRec.y1(), box_msg.box[2]=tRec.x2(), box_msg.box[3]=tRec.y2(),box_msg.box[4]=tRec.cos2theta(), box_msg.box[5]=tRec.sin2theta();
				 
			// check if box exist
			if(!check_box_exist(box_msg, temp_list) && check_point_valid(box_msg)){
				// calculate final score
				box_msg.score = rec_score[di]/max_score;
				// response
				temp_list.push_back(box_msg);
			}
		}
	}
	
	// sort boxes with scores
	int total_box = temp_list.size();
	for(int j=0; j<total_box; ++j){
		float max_s = FLT_MIN;
		int idx;
		for (int k=0; k<temp_list.size();++k){
			if(max_s < temp_list[k].score){
				idx = k;
				max_s = temp_list[k].score;
			}
		}
		deep_rec::BoxMsg b = temp_list[idx];
		res.boxes.push_back(b);
		temp_list.erase(temp_list.begin()+idx);
	}
	
	
	
	ROS_INFO("vector size: %d", res.boxes.size());
	
	//--------------------
	// -----Main loop-----
	//--------------------
	/*while (!viewer.wasStopped ())
	{
	range_image_widget.spinOnce ();  // process GUI events
	viewer.spinOnce ();
	pcl_sleep(0.01);
	}*/
	
	return true;						
	
}
