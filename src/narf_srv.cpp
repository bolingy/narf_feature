#include <depth_feature/depth_feature.hpp>




int main (int argc, char**argv){

	
	ros::init(argc, argv, "depth_feature");
	ros::NodeHandle node_handle;

	//ros::ServiceServer service = node_handle.advertiseService("narf_rect", narf_rect_detection);
	ros::ServiceServer service = node_handle.advertiseService("narf_rect", narf_sparse_detection);
	ROS_INFO("narf detector started");
	ros::spin();
	
	return 0;
}



