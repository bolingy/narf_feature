#include <iostream>
#include <iterator>

#include <stdio.h>
#include <math.h>
#include <ros/ros.h>

#include <sensor_msgs/JointState.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/CompressedImage.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/point_cloud_conversion.h>

#include <boost/thread/thread.hpp>
#include <pcl/range_image/range_image.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/range_image_visualizer.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/features/range_image_border_extractor.h>
#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/features/narf_descriptor.h>
#include <pcl/console/parse.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/point_cloud_handlers.h>

#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl_conversions/pcl_conversions.h>

#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf2/convert.h>
#include <Eigen/Geometry>
#include <geometry_msgs/PointStamped.h>
#include <geometry_msgs/PoseStamped.h>

#include <depth_feature/narf_rect.h>

#include <data_collection/collect_data.h>
#include <self_supervision/cloud_analyzer.h>

typedef pcl::PointXYZ PointType;

float maxAngleWidth = (float) (57.0f * (M_PI / 180.0f));
float maxAngleHeight = (float) (43.0f * (M_PI / 180.0f));
float angularResolution = (float)(57.0f / 640.0f * (M_PI/180.0f));

float angular_resolution = pcl::deg2rad (0.5f);
pcl::RangeImage::CoordinateFrame coordinate_frame = pcl::RangeImage::CAMERA_FRAME;
bool setUnseenToMaxRange = false ;
bool rotation_invariant = false;

void onePoint2Pix(const sensor_msgs::CameraInfo& camera_info,
				PointType& point,
				vector<double>& pix);
				
int getMinIndex(vector<double>& temp);
int getMaxIndex(vector<double>& temp);
void getOrder(const vector<double>& ins_pix,
				const vector<double>& res_pix,
				const vector<double>& orth_pix, 
				const vector<double>& diag_pix,
				vector<float>& xy);
bool check_point_valid(deep_rec::BoxMsg box_msg);
void getCenterPix(vector<float>& xy, float& cx, float& cy);
void rotatePoint(float cx, float cy, float angle, float& x, float& y);
TwoPointRectangle pixTwoPointsRec( vector<float>& xy);
int filter_table(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);
void setViewerPose (pcl::visualization::PCLVisualizer& viewer, const Eigen::Affine3f& viewer_pose);
bool narf_rect_detection(depth_feature::narf_rect::Request &req,
						depth_feature::narf_rect::Response &res);
bool narf_sparse_detection(depth_feature::narf_rect::Request &req,
						depth_feature::narf_rect::Response &res);
bool check_box_exist(deep_rec::BoxMsg msg, vector<deep_rec::BoxMsg> vec);
void filter_rangePoint(pcl::PointCloud<pcl::PointXYZ>& kp,
						pcl::PointCloud<int>& keypoint_indices,
						pcl::RangeImage& range_image);
void sparsePoint(const pcl::PointCloud<PointType>& input_cloud,
					pcl::PointCloud<pcl::PointXYZ>& keypoints,
					float distance,
					const std::vector<int>& desp_dir,
					pcl::PointCloud<PointType>& result_cloud);
void cloudForPix(const pcl::PointCloud<PointType>& result_cloud, 
					const pcl::PointCloud<PointType>& orth_cloud,
					 std::vector< pcl::PointCloud<PointType> >& pix_clouds);
void orthPoint(const pcl::PointCloud<PointType>& input_cloud,
					pcl::PointCloud<pcl::PointXYZ>& keypoints,
					const pcl::PointCloud<pcl::Narf36>& narf_descriptors,
					float distance,
					const std::vector<int>& desp_dir,
					pcl::PointCloud<PointType>& orth_cloud,
					pcl::PointCloud<PointType>& diag_cloud,
					std::vector<float>& rec_score); 
